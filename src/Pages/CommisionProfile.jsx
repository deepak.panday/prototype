import * as React from "react";
import PropTypes from "prop-types";
import { Tabs, InputAdornment, Tab, Checkbox } from "@mui/material";
import Typography from "@mui/material/Typography";
import { alpha, styled } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import {
  Box,
  FormControl,
  InputLabel,
  TextField,
  MenuItem,
} from "@mui/material";
import { Nav, Row, Col } from "react-bootstrap";
import { FaChevronUp, FaRegCalendarAlt, FaPencilAlt } from "react-icons/fa";

import { AiOutlineSetting, AiOutlineStop } from "react-icons/ai";
import { FiTrash2, FiPlusCircle, FiSearch } from "react-icons/fi";

const currencies = [
  {
    value: "USD",
    label: "All",
  },
  {
    value: "EUR",
    label: "All",
  },
  {
    value: "BTC",
    label: "All",
  },
  {
    value: "JPY",
    label: "All",
  },
];
const counts = [
  {
    value: "1",
    label: "1",
  },
  {
    value: "2",
    label: "2",
  },
  {
    value: "3",
    label: "3",
  },
  {
    value: "4",
    label: "4",
  },
];
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const CommisionProfile = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const BootstrapInput = styled(InputBase)(({ theme }) => ({
    "label + &": {
      marginTop: theme.spacing(3),
    },
    "& .MuiInputBase-input": {
      borderRadius: 4,
      position: "relative",
      backgroundColor: theme.palette.mode === "light" ? "#fcfcfb" : "#2b2b2b",
      border: "1px solid #ced4da",
      fontSize: 16,
      width: "auto",
      padding: "10px 12px",
      transition: theme.transitions.create([
        "border-color",
        "background-color",
        "box-shadow",
      ]),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(","),
      "&:focus": {
        boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
        borderColor: theme.palette.primary.main,
      },
    },
  }));
  const label = { inputProps: { "aria-label": "Checkbox demo" } };

  return (
    <>
      <div className="">
        <Nav
          className="justify-content-end align-items-center"
          activeKey="/home"
        >
          <Nav.Item>
            <Nav.Link className="color-767677" eventKey="link-1">
              Help
            </Nav.Link>
          </Nav.Item>
          <Nav.Item className="color-AFAFB1">|</Nav.Item>
          <Nav.Item>
            <Nav.Link className="color-767677 " eventKey="link-2">
              Network Admin
              <span className="user-name-icon">N</span>
            </Nav.Link>
          </Nav.Item>
        </Nav>
        <Box sx={{ width: "100%" }}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
            >
              <Tab label="E TopUp" {...a11yProps(3)} />
              <Tab label="VMS" {...a11yProps(3)} />
            </Tabs>
          </Box>
          <TabPanel value={value} index={0}>
            Item One
          </TabPanel>
          <TabPanel value={value} index={1}>
            Item Two
          </TabPanel>
        </Box>

        <Box sx={{ width: "100%" }}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
            >
              <Tab label="Single Opration" {...a11yProps(0)} />
              <Tab label="Bulk Opration" {...a11yProps(1)} />
            </Tabs>
          </Box>
          <TabPanel value={value} index={0}>
            <Box>
              <h5 className="mb-3">Commision Profile</h5>
              <div className="card-cs p-4">
                <div className="d-flex align-items-center justify-content-between">
                  <h5 className="mb-0">Filters</h5>
                  <p className="mb-0">
                    Hide <FaChevronUp />
                  </p>
                </div>
                <div className="mt-3">
                  <Row>
                    <Col md={2}>
                      <label>Domain</label>
                      <TextField
                        fullWidth
                        id="outlined-select-currency"
                        select
                        defaultValue="EUR"
                      >
                        {currencies.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Col>

                    <Col md={2}>
                      <label>Category</label>
                      <TextField
                        fullWidth
                        id="outlined-select-currency"
                        select
                        defaultValue="EUR"
                      >
                        {currencies.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Col>

                    <Col md={2}>
                      <label>Geography</label>
                      <TextField
                        fullWidth
                        id="outlined-select-currency"
                        select
                        defaultValue="EUR"
                      >
                        {currencies.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Col>

                    <Col md={2}>
                      <label>Grade</label>
                      <TextField
                        fullWidth
                        id="outlined-select-currency"
                        select
                        defaultValue="EUR"
                      >
                        {currencies.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Col>

                    <Col md={1}>
                      <label>Status</label>

                      <TextField
                        fullWidth
                        id="outlined-select-currency"
                        select
                        defaultValue="EUR"
                      >
                        {currencies.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Col>

                    <Col md={3}>
                      <label>Applicable from (Date and time)</label>
                      <TextField
                        required
                        id="outlined-required"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <FaRegCalendarAlt />
                            </InputAdornment>
                          ),
                        }}
                      />
                    </Col>

                    {/* <Col md={2}>
                      <FormControl fullWidth variant="standard">
                      <label>Status</label>

                        <InputLabel shrink htmlFor="bootstrap-input">
                          Bootstrap
                        </InputLabel>
                        <BootstrapInput
                          defaultValue="react-bootstrap"
                          id="bootstrap-input"
                        />
                      </FormControl>
                    </Col> */}
                  </Row>
                </div>
                <div className="text-end mt-3">
                  <button className="secondary-btn me-3">RESET</button>
                  <button className="primary-btn">PROCEED</button>
                </div>
              </div>
            </Box>
            <Box className="mt-3">
              <div className="card-cs p-4">
                <div className="d-flex align-items-center mb-4">
                  <h5 className="me-3">Commision Profile</h5>
                  <button className="third-btn d-flex align-items-center">
                    <FiPlusCircle className="me-2" /> Add Commision Profile
                  </button>
                </div>
                <Col md={6}>
                  <TextField
                  fullWidth
                    required
                    id="outlined-required"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <FiSearch />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Col>
                <div className=" table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">
                          <Checkbox {...label} />
                        </th>
                        <th scope="col">Profile set</th>
                        <th scope="col">Domain</th>
                        <th scope="col">Category</th>
                        <th scope="col">Geography</th>
                        <th scope="col">Commision Type</th>
                        <th scope="col">Version</th>
                        <th scope="col">Applicable Form</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">
                          <Checkbox {...label} />
                        </th>
                        <td>
                          <a className="" href="#">
                            {" "}
                            Compro1
                          </a>
                        </td>
                        <td>Distributer</td>
                        <td>Super Distributer</td>
                        <td>Haryana</td>
                        <td>Normal</td>
                        <td>
                          <TextField
                            fullWidth
                            id="outlined-select-currency"
                            select
                            defaultValue="1"
                          >
                            {counts.map((option) => (
                              <MenuItem key={option.value} value={option.value}>
                                {option.label}
                              </MenuItem>
                            ))}
                          </TextField>
                        </td>
                        <td>
                          <date>24-04-2019</date>
                          <time>09:10:17</time>
                        </td>
                        <td>
                          <span className="active-text color-71CE7B">
                            Active
                          </span>
                        </td>
                        <td>
                          <FaPencilAlt className="tbl-icon" />
                          <AiOutlineSetting className="tbl-icon" />
                          <AiOutlineStop className="tbl-icon" />
                          <FiTrash2 className="tbl-icon" />
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <Checkbox {...label} />
                        </th>
                        <td>
                          <a className="" href="#">
                            {" "}
                            Compro1
                          </a>
                        </td>
                        <td>Distributer</td>
                        <td>Super Distributer</td>
                        <td>Haryana</td>
                        <td>Normal</td>
                        <td>
                          <TextField
                            fullWidth
                            id="outlined-select-currency"
                            select
                            defaultValue="1"
                          >
                            {counts.map((option) => (
                              <MenuItem key={option.value} value={option.value}>
                                {option.label}
                              </MenuItem>
                            ))}
                          </TextField>
                        </td>
                        <td>
                          <date>24-04-2019</date>
                          <time>09:10:17</time>
                        </td>
                        <td>
                          <span className="active-text color-71CE7B">
                            Active
                          </span>
                        </td>
                        <td>
                          <FaPencilAlt className="tbl-icon" />
                          <AiOutlineSetting className="tbl-icon" />
                          <AiOutlineStop className="tbl-icon" />
                          <FiTrash2 className="tbl-icon" />
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <Checkbox {...label} />
                        </th>
                        <td>
                          <a className="" href="#">
                            {" "}
                            Compro1
                          </a>
                        </td>
                        <td>Distributer</td>
                        <td>Super Distributer</td>
                        <td>Haryana</td>
                        <td>Normal</td>
                        <td>
                          <TextField
                            fullWidth
                            id="outlined-select-currency"
                            select
                            defaultValue="1"
                          >
                            {counts.map((option) => (
                              <MenuItem key={option.value} value={option.value}>
                                {option.label}
                              </MenuItem>
                            ))}
                          </TextField>
                        </td>
                        <td>
                          <date>24-04-2019</date>
                          <time>09:10:17</time>
                        </td>
                        <td>
                          <span className="active-text color-71CE7B">
                            Active
                          </span>
                        </td>
                        <td>
                          <FaPencilAlt className="tbl-icon" />
                          <AiOutlineSetting className="tbl-icon" />
                          <AiOutlineStop className="tbl-icon" />
                          <FiTrash2 className="tbl-icon" />
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <Checkbox {...label} />
                        </th>
                        <td>
                          <a className="" href="#">
                            {" "}
                            Compro1
                          </a>
                        </td>
                        <td>Distributer</td>
                        <td>Super Distributer</td>
                        <td>Haryana</td>
                        <td>Normal</td>
                        <td>
                          <TextField
                            fullWidth
                            id="outlined-select-currency"
                            select
                            defaultValue="1"
                          >
                            {counts.map((option) => (
                              <MenuItem key={option.value} value={option.value}>
                                {option.label}
                              </MenuItem>
                            ))}
                          </TextField>
                        </td>
                        <td>
                          <date>24-04-2019</date>
                          <time>09:10:17</time>
                        </td>
                        <td>
                          <span className="active-text color-71CE7B">
                            Active
                          </span>
                        </td>
                        <td>
                          <FaPencilAlt className="tbl-icon" />
                          <AiOutlineSetting className="tbl-icon" />
                          <AiOutlineStop className="tbl-icon" />
                          <FiTrash2 className="tbl-icon" />
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <Checkbox {...label} />
                        </th>
                        <td>
                          <a className="" href="#">
                            {" "}
                            Compro1
                          </a>
                        </td>
                        <td>Distributer</td>
                        <td>Super Distributer</td>
                        <td>Haryana</td>
                        <td>Normal</td>
                        <td>
                          <TextField
                            fullWidth
                            id="outlined-select-currency"
                            select
                            defaultValue="1"
                          >
                            {counts.map((option) => (
                              <MenuItem key={option.value} value={option.value}>
                                {option.label}
                              </MenuItem>
                            ))}
                          </TextField>
                        </td>
                        <td>
                          <date>24-04-2019</date>
                          <time>09:10:17</time>
                        </td>
                        <td>
                          <span className="active-text color-71CE7B">
                            Active
                          </span>
                        </td>
                        <td>
                          <FaPencilAlt className="tbl-icon" />
                          <AiOutlineSetting className="tbl-icon" />
                          <AiOutlineStop className="tbl-icon" />
                          <FiTrash2 className="tbl-icon" />
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <Checkbox {...label} />
                        </th>
                        <td>
                          <a className="" href="#">
                            {" "}
                            Compro1
                          </a>
                        </td>
                        <td>Distributer</td>
                        <td>Super Distributer</td>
                        <td>Haryana</td>
                        <td>Normal</td>
                        <td>
                          <TextField
                            fullWidth
                            id="outlined-select-currency"
                            select
                            defaultValue="1"
                          >
                            {counts.map((option) => (
                              <MenuItem key={option.value} value={option.value}>
                                {option.label}
                              </MenuItem>
                            ))}
                          </TextField>
                        </td>
                        <td>
                          <date>24-04-2019</date>
                          <time>09:10:17</time>
                        </td>
                        <td>
                          <span className="active-text color-71CE7B">
                            Active
                          </span>
                        </td>
                        <td>
                          <FaPencilAlt className="tbl-icon" />
                          <AiOutlineSetting className="tbl-icon" />
                          <AiOutlineStop className="tbl-icon" />
                          <FiTrash2 className="tbl-icon" />
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <Checkbox {...label} />
                        </th>
                        <td>
                          <a className="" href="#">
                            {" "}
                            Compro1
                          </a>
                        </td>
                        <td>Distributer</td>
                        <td>Super Distributer</td>
                        <td>Haryana</td>
                        <td>Normal</td>
                        <td>
                          <TextField
                            fullWidth
                            id="outlined-select-currency"
                            select
                            defaultValue="1"
                          >
                            {counts.map((option) => (
                              <MenuItem key={option.value} value={option.value}>
                                {option.label}
                              </MenuItem>
                            ))}
                          </TextField>
                        </td>
                        <td>
                          <date>24-04-2019</date>
                          <time>09:10:17</time>
                        </td>
                        <td>
                          <span className="active-text color-71CE7B">
                            Active
                          </span>
                        </td>
                        <td>
                          <FaPencilAlt className="tbl-icon" />
                          <AiOutlineSetting className="tbl-icon" />
                          <AiOutlineStop className="tbl-icon" />
                          <FiTrash2 className="tbl-icon" />
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <Checkbox {...label} />
                        </th>
                        <td>
                          <a className="" href="#">
                            {" "}
                            Compro1
                          </a>
                        </td>
                        <td>Distributer</td>
                        <td>Super Distributer</td>
                        <td>Haryana</td>
                        <td>Normal</td>
                        <td>
                          <TextField
                            fullWidth
                            id="outlined-select-currency"
                            select
                            defaultValue="1"
                          >
                            {counts.map((option) => (
                              <MenuItem key={option.value} value={option.value}>
                                {option.label}
                              </MenuItem>
                            ))}
                          </TextField>
                        </td>
                        <td>
                          <date>24-04-2019</date>
                          <time>09:10:17</time>
                        </td>
                        <td>
                          <span className="active-text color-71CE7B">
                            Active
                          </span>
                        </td>
                        <td>
                          <FaPencilAlt className="tbl-icon" />
                          <AiOutlineSetting className="tbl-icon" />
                          <AiOutlineStop className="tbl-icon" />
                          <FiTrash2 className="tbl-icon" />
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </Box>
          </TabPanel>
          <TabPanel value={value} index={1}>
            Item Two
          </TabPanel>
        </Box>
      </div>
    </>
  );
};

export default CommisionProfile;
