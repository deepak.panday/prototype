import * as React from "react";
import PropTypes from "prop-types";
import { Tabs, InputAdornment, Tab, Checkbox } from "@mui/material";
import Typography from "@mui/material/Typography";
import { alpha, styled } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import {
  Box,
  FormControl,
  InputLabel,
  TextField,
  MenuItem,
  FormGroup,
  FormControlLabel,
} from "@mui/material";
import { Nav, Row, Col } from "react-bootstrap";
import { FaChevronUp, FaRegCalendarAlt, FaRegCopy } from "react-icons/fa";

import { AiOutlineSetting, AiOutlineStop } from "react-icons/ai";
import { FiTrash2, FiPlusCircle, FiSearch } from "react-icons/fi";

const currencies = [
  {
    value: "USD",
    label: "All",
  },
  {
    value: "EUR",
    label: "All",
  },
  {
    value: "BTC",
    label: "All",
  },
  {
    value: "JPY",
    label: "All",
  },
];
const counts = [
  {
    value: "1",
    label: "1",
  },
  {
    value: "2",
    label: "2",
  },
  {
    value: "3",
    label: "3",
  },
  {
    value: "4",
    label: "4",
  },
];
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const AddCommisionProfile = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const BootstrapInput = styled(InputBase)(({ theme }) => ({
    "label + &": {
      marginTop: theme.spacing(3),
    },
    "& .MuiInputBase-input": {
      borderRadius: 4,
      position: "relative",
      backgroundColor: theme.palette.mode === "light" ? "#fcfcfb" : "#2b2b2b",
      border: "1px solid #ced4da",
      fontSize: 16,
      width: "auto",
      padding: "10px 12px",
      transition: theme.transitions.create([
        "border-color",
        "background-color",
        "box-shadow",
      ]),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(","),
      "&:focus": {
        boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
        borderColor: theme.palette.primary.main,
      },
    },
  }));
  const label = { inputProps: { "aria-label": "Checkbox demo" } };

  return (
    <>
      <div className="">
        <Nav
          className="justify-content-end align-items-center"
          activeKey="/home"
        >
          <Nav.Item>
            <Nav.Link className="color-767677" eventKey="link-1">
              Help
            </Nav.Link>
          </Nav.Item>
          <Nav.Item className="color-AFAFB1">|</Nav.Item>
          <Nav.Item>
            <Nav.Link className="color-767677 " eventKey="link-2">
              Network Admin
              <span className="user-name-icon">N</span>
            </Nav.Link>
          </Nav.Item>
        </Nav>

        <Box>
          <div className="card-cs p-4 m-4">
            <div className="d-flex align-items-center justify-content-between">
              <h5 className="mb-0">Filters</h5>
              <p className="mb-0">
                Hide <FaChevronUp />
              </p>
            </div>
            <div className="mt-3">
              <Row>
                <Col md={3} className="mb-2">
                  <label>Domain</label>
                  <TextField
                    fullWidth
                    id="outlined-select-currency"
                    select
                    defaultValue="EUR"
                  >
                    {currencies.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </Col>

                <Col md={3} className="mb-2">
                  <label>Category</label>
                  <TextField
                    fullWidth
                    id="outlined-select-currency"
                    select
                    defaultValue="EUR"
                  >
                    {currencies.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </Col>

                <Col md={3} className="mb-2">
                  <label>Geography</label>
                  <TextField
                    fullWidth
                    id="outlined-select-currency"
                    select
                    defaultValue="EUR"
                  >
                    {currencies.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </Col>

                <Col md={2}>
                  <label>Grade</label>
                  <TextField
                    fullWidth
                    id="outlined-select-currency"
                    select
                    defaultValue="EUR"
                  >
                    {currencies.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </Col>
                <Col md={3}>
                  <label>Profile Name</label>
                  <TextField fullWidth required id="outlined-required" />
                </Col>
                <Col md={3}>
                  <label>Short Code</label>
                  <TextField fullWidth required id="outlined-required" />
                </Col>
                <Col md={3}>
                  <label>Commision Type</label>

                  <TextField
                    fullWidth
                    id="outlined-select-currency"
                    select
                    defaultValue="EUR"
                  >
                    {currencies.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </Col>

                <Col md={3}>
                  <label>Applicable from (Date and time)</label>
                  <TextField
                    required
                    id="outlined-required"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <FaRegCalendarAlt />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Col>

                {/* <Col md={2}>
                      <FormControl fullWidth variant="standard">
                      <label>Status</label>

                        <InputLabel shrink htmlFor="bootstrap-input">
                          Bootstrap
                        </InputLabel>
                        <BootstrapInput
                          defaultValue="react-bootstrap"
                          id="bootstrap-input"
                        />
                      </FormControl>
                    </Col> */}
              </Row>
            </div>
          </div>
        </Box>

        <Box sx={{ width: "100%" }}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
            >
              <Tab label="Base Commission" {...a11yProps(0)} />
              <Tab label="Cumulative Base Commission" {...a11yProps(1)} />
              <Tab label="Additional Commission" {...a11yProps(2)} />
              <Tab label="Other Commission" {...a11yProps(3)} />
            </Tabs>
          </Box>
          <TabPanel value={value} index={0}>
            <Box className="mt-3">
              <div className="card-cs p-4">
                <div className="mb-3">
                  <h6>Select transaction type</h6>
                  <div className="mt-3">
                    <ul className="tag-card m-0 p-0">
                      <li>Default</li>
                      <li>Operator to Channel</li>
                      <li>Operator to Channel</li>
                    </ul>
                  </div>
                </div>

                <div className="mb-3">
                  <h6>Select product(s)</h6>
                  <div className="mt-3">
                    <ul className="tag-card m-0 p-0">
                      <li>Prepaid eTopUp</li>
                      <li>Postpaid eTopUp</li>
                      <li>Postpaid eTopUp2</li>
                    </ul>
                  </div>
                </div>

                <div className="mb-3">
                  <h6>Select Payment mode(s)</h6>
                  <div className="mt-3">
                    <ul className="tag-card m-0 p-0">
                      <li>Select all</li>
                      <li>Default</li>
                      <li>Cash</li>
                      <li>DD</li>
                      <li>Online</li>
                      <li>Cheque</li>
                      <li>Other</li>
                    </ul>
                  </div>
                </div>

                <hr />
                <Row>
                  <Col md={6}>
                    <Row>
                      <Col md={2}>
                        <label className="mb-4">Multiple of</label>
                        <TextField fullWidth required id="outlined-required" />
                      </Col>
                      <Col md={4}>
                        <label>Transfer value</label>
                        <div className="d-flex">
                          <div className="">
                            <label>Min</label>
                            <TextField
                              fullWidth
                              required
                              id="outlined-required"
                            />
                          </div>
                          <div className="">
                            <label>Max</label>
                            <TextField
                              fullWidth
                              required
                              id="outlined-required"
                            />
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </Col>
                  <Col md={6}>
                    <div className="text-end">
                      <button className="fourth-btn">PASTE COPIED CARDS</button>
                    </div>
                  </Col>
                </Row>
                <div className="my-3">
                  <FormGroup>
                    <FormControlLabel
                      control={<Checkbox />}
                      label="Tax calculation on Free of Cost(FOC)"
                    />
                    <FormControlLabel
                      control={<Checkbox />}
                      label="Tax calculation on Channel to Channel Transfer"
                    />
                  </FormGroup>
                </div>
                <Box className="add-m-box borderd">
                  <div className="ff-form">
                    <div className="table-resposive">
                      <table class="table table-borderless">
                        <thead>
                          <tr>
                            <th scope="col">Transaction amount range</th>
                            <th scope="col">Select commission rate and unit</th>
                            <th scope="col">Tax 1</th>
                            <th scope="col">Tax 2</th>
                            <th scope="col">Tax 3</th>
                            <th scope="col">
                              <FaRegCopy className="color-767677" />
                              <FiTrash2 className="color-AFAFB1" />
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th>
                              <Col md={8}>
                                <div className="d-flex">
                                  <TextField
                                    fullWidth
                                    required
                                    id="outlined-required"
                                  />
                                  <TextField
                                    fullWidth
                                    required
                                    id="outlined-required"
                                  />
                                </div>
                              </Col>
                            </th>
                            <td>
                              <Col md={8}>
                                <div className="d-flex">
                                  <TextField
                                    fullWidth
                                    required
                                    id="outlined-required"
                                  />
                                  <TextField
                                    fullWidth
                                    required
                                    id="outlined-required"
                                  />
                                </div>
                              </Col>
                            </td>
                            <td>
                              <Col md={8}>
                                <div className="d-flex">
                                  <TextField
                                    fullWidth
                                    required
                                    id="outlined-required"
                                  />
                                  <TextField
                                    fullWidth
                                    required
                                    id="outlined-required"
                                  />
                                </div>
                              </Col>
                            </td>
                            <td>
                              <Col md={8}>
                                <div className="d-flex">
                                  <TextField
                                    fullWidth
                                    required
                                    id="outlined-required"
                                  />
                                  <TextField
                                    fullWidth
                                    required
                                    id="outlined-required"
                                  />
                                </div>
                              </Col>
                            </td>
                            <td>
                              <Col md={8}>
                                <div className="d-flex">
                                  <TextField
                                    fullWidth
                                    required
                                    id="outlined-required"
                                  />
                                  <TextField
                                    fullWidth
                                    required
                                    id="outlined-required"
                                  />
                                </div>
                              </Col>
                            </td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                      <div className="text-end">
                        <button className="fourth-btn">RESET</button>
                      </div>
                     
                    </div>
                    
                  </div>
                  <div className="mt-3">
                        <button className="link-btn d-flex align-items-center">
                          <FiPlusCircle className="me-3" /> ADD RANGE
                        </button>
                      </div>
                </Box>
                <div className="text-end">
                  <button className="fourth-btn me-2">COPY</button>
                  <button className="secondary-btn me-2">ADD</button>
                  <button className="third-btn">ADD</button>
                </div>
              </div>
              <div className="text-end mt-4 mb-3">
                <button className="link-btn me-3">
                <span className=" d-flex align-items-center">
                <FiPlusCircle className="me-3" /> ADD NEW CARD
                </span>
                  
                </button>
                <button className="fourth-btn me-2">SAVE & SUBMIT</button>
              </div>
            </Box>
          </TabPanel>
          <TabPanel value={value} index={1}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={2}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={3}>
            Item Two
          </TabPanel>
        </Box>
      </div>
    </>
  );
};

export default AddCommisionProfile;
