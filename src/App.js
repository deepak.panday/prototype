import React from "react";
import "./App.css";
import "./style.css";
import { Routes, Route } from 'react-router';
import CommisionProfile from "./Pages/CommisionProfile"
import AddCommisionProfile from "./Pages/AddCommisionProfile"


function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<CommisionProfile/>} />
        <Route path="/add-commision-profile" element={<AddCommisionProfile/>} />
      </Routes>
    </>
  );
}

export default App;
